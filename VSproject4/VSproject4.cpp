﻿#include <iostream>
#include "windows.h"
#include <math.h>
#include <string>
using namespace std;
void PrintNumbers(int N, bool Even)
{
	int a;
	std::string parity;
	if (Even)
	{
		parity = "Чётные";
		a = 0;
	}
	else
	{
		parity = "Нечётные";
		a = 1;
	}
	for (;a < N; a = a + 2)
	{	
		cout << parity << a << endl;
	}
}

int main()
{
	int n;
	setlocale(0, "Rus");
	cout << "введите число:";
	cin >> n;
	PrintNumbers(5, true);
	PrintNumbers(10, false);

	system("pause");
	return 0;
}	